# Тестовое задание на python разработчика Совкомбанк

## Project setup
Все необходимые пакеты описаны в Pipfile
### 1) Установка пакетов используя pipenv:
```
pipenv install
```
### 2) Migrate модели в sqlite3 БД
```
python manage.py migrate
```

### 3) Создать пользователя админки
```
python manage.py createsuperuser
```

### 4) Запуск сервера
```
python manage.py runserver
```
