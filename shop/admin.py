from django.contrib import admin
from shop.models import Sale, Product


admin.site.register(Sale)
admin.site.register(Product)
