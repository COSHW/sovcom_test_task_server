from django.db import models
from datetime import date


class Product(models.Model):
    # Name of a product.
    name = models.CharField(max_length=256, verbose_name="Name")
    # Price of a product in rubles
    price = models.PositiveIntegerField(verbose_name="Price")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Product"


class Sale(models.Model):
    """
    Sale of a product.
    """
    # Date of a sale or order
    date = models.DateField(default=date.today, verbose_name="Date")
    # Amount of bought products
    amount = models.IntegerField(verbose_name="Amount")
    # Sold product FK.
    product = models.ForeignKey(Product, on_delete=models.PROTECT, verbose_name='Product', related_name="sales")

    def __str__(self):
        return self.date

    class Meta:
        verbose_name = "Sale"

