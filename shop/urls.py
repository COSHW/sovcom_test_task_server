from django.urls import path

from .views import LoginView, ProductListAPIView, SaleListAPIView, ValidateCredsView, LoginRedirectView, Products, \
    Sales, CreateSale, ChangeSale

urlpatterns = [
    # interface
    path('', LoginRedirectView.as_view()),
    path('login', LoginView.as_view()),
    path('validate', ValidateCredsView.as_view()),
    path('sales', Sales.as_view()),
    path('create-sale', CreateSale.as_view()),
    path('change-sale/<int:_id>', ChangeSale.as_view()),
    path('products', Products.as_view()),

    # REST
    path('api/products', ProductListAPIView.as_view()),
    path('api/sales', SaleListAPIView.as_view()),
]
