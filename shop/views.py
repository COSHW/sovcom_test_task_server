from django.contrib import auth
from django.contrib.auth.models import update_last_login
from django.db.models import Sum, Q, F
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.views.generic import RedirectView
from rest_framework import generics

from rest_framework.permissions import IsAuthenticated
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.response import Response
from rest_framework.views import APIView

from shop.models import Product, Sale
from shop.serializers import ProductSerializer, SaleSerializer, LoginSerializers


# REST
class ProductListAPIView(generics.ListCreateAPIView):
    serializer_class = ProductSerializer
    permission_classes = (IsAuthenticated, )

    def get_queryset(self):
        return Product.objects.all()


class SaleListAPIView(generics.ListCreateAPIView):
    serializer_class = SaleSerializer
    permission_classes = (IsAuthenticated, )

    def get_queryset(self):
        return Sale.objects.all()


class ValidateCredsView(APIView):
    def post(self, request, *args, **kwargs):
        serializer = LoginSerializers(data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        update_last_login(None, user)
        auth.login(request, user)
        # token, created = Token.objects.get_or_create(user=user)
        return HttpResponseRedirect(redirect_to='/products')


# Views
class LoginRedirectView(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        return '/login'


class LoginView(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'login.html'

    def get(self, request):
        serializer = LoginSerializers()
        return ORJsonResponse({'serializer': serializer})


class Products(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'products.html'

    def get(self, request, *args, **kwargs):
        products = Product.objects.annotate(profit=Sum(F('sales__amount') * F('price'), distinct=True)).all()
        return ORJsonResponse({"products": products})


class Sales(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'sales.html'

    def get(self, request, *args, **kwargs):
        sales = Sale.objects.annotate(sum=Sum(F('amount') * F('product__price'), distinct=True)).all()
        return ORJsonResponse({"sales": sales})


class CreateSale(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'create_sale.html'

    def get(self, request, _id=None, *args, **kwargs):
        serializer = SaleSerializer()
        if _id:
            serializer = SaleSerializer(instance=get_object_or_404(Sale, pk=_id))
        return ORJsonResponse({'serializer': serializer})

    def post(self, request, *args, **kwargs):
        serializer = SaleSerializer(data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return HttpResponseRedirect(redirect_to='/sales')


class ChangeSale(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'change-sale.html'

    def get(self, request, _id, *args, **kwargs):
        serializer = SaleSerializer(instance=get_object_or_404(Sale, pk=_id))
        return ORJsonResponse({'serializer': serializer, 'id': _id})

    def post(self, request, _id, *args, **kwargs):
        instance = get_object_or_404(Sale, pk=_id)
        serializer = SaleSerializer(instance=instance, data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return HttpResponseRedirect(redirect_to='/sales')
