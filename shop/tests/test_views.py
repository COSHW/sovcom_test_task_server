from rest_framework.authtoken.admin import User
from rest_framework.test import APITestCase


class TestLoginPage(APITestCase):

    def setUp(self):
        self.user = User.objects.create_user(username='test', first_name='test', last_name='test',
                                             email='test@gmail.com', password='Test1234')

    def test_get_page(self):
        response = self.client.get("/login")
        self.assertEqual(response.status_code, 200)

    def test_validate_creds(self):
        response = self.client.post("/validate", data=self.login_data())
        self.assertEqual(response.status_code, 302)

    def login_data(self) -> dict:
        return {"username": "test",
                "password": "Test1234"}


class TestProductsPage(APITestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='test', first_name='test', last_name='test',
                                             email='test@gmail.com', password='Test1234')
        self.client.login(username=self.user.username, password='Test1234')

    def test_get_page(self):
        response = self.client.get("/products")
        self.assertEqual(response.status_code, 200)

