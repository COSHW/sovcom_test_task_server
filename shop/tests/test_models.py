from django.test import TestCase
from django.db.utils import IntegrityError
from shop.models import Sale, Product


class SaleTest(TestCase):
    def setUp(self) -> None:
        self.product_stub = Product.objects.create(name="test", price=10)

    def test_create(self) -> None:
        test_sale_full = Sale(date='2022-10-10', amount=5, product=self.product_stub)
        test_sale_full.save()

    def test_create_fail(self) -> None:
        test_sale_none = Sale()
        with self.assertRaises(IntegrityError):
            test_sale_none.save()


class ProductTest(TestCase):
    def test_creation(self) -> None:
        test_product_full = Product(name="test", price=500)
        test_product_full.save()

    def test_create_fail(self) -> None:
        test_product_none = Product()
        with self.assertRaises(IntegrityError):
            test_product_none.save()
