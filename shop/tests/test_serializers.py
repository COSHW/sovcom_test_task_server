from django.test import TestCase
from shop.models import Product
from shop.serializers import SaleSerializer, ProductSerializer


class SaleSerializerTest(TestCase):
    def setUp(self) -> None:
        self.product_stub = Product.objects.create(name="test", price=10)

    def test_create(self) -> None:
        test_sale_full = SaleSerializer(data={"date": '2022-10-10', "amount": 5, "product_id": self.product_stub.id})
        test_sale_full.is_valid(raise_exception=True)
        test_sale_full.save()

    def test_create_fail(self) -> None:
        test_sale_none = SaleSerializer()
        with self.assertRaises(AssertionError):
            test_sale_none.is_valid(raise_exception=True)


class ProductTest(TestCase):
    def test_creation(self) -> None:
        test_product_full = ProductSerializer(data={"name": "test", "price": 500})
        test_product_full.is_valid(raise_exception=True)
        test_product_full.save()

    def test_create_fail(self) -> None:
        test_product_none = ProductSerializer()
        with self.assertRaises(AssertionError):
            test_product_none.is_valid(raise_exception=True)
